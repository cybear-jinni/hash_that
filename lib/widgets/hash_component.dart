
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:hash_that/services/hash_service.dart';

class HashComponent extends StatefulWidget {
  const HashComponent({super.key});

  @override
  State<HashComponent> createState() => _HashComponentState();
}

class _HashComponentState extends State<HashComponent> {
  final TextEditingController _textController = TextEditingController();
  String _hashedText = '';

  void _hashText() {
    setState(() {
      _hashedText = HashService.hashText(_textController.text);
    });
  }

  @override
  void dispose() {
    _textController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: TextField(
                controller: _textController,
                decoration: const InputDecoration(labelText: 'Enter text'),
              ),
            ),
            ElevatedButton(
              onPressed: _hashText,
              child:  const Text('hash_text').tr(),
            ),
            const SizedBox(height: 18.0),
            if (_hashedText.isNotEmpty)
              SelectableText(
                tr('hashed_text') + _hashedText,
                style: const TextStyle(fontSize: 16.0),
              ),
          ],



        ),
      ),
    );
  }
}
